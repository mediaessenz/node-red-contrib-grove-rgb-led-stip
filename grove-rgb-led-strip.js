var path = require("path");

module.exports = function (RED) {
  function GroveRgbLedStrip(config) {
    RED.nodes.createNode(this, config);

    this.port_name = "D" + config.port;
    this.status({ fill: "blue", shape: "dot", text: this.port_name });

    var node = this;
    node.on("input", function (msg) {
      const exec = require("child_process").exec;
      this.status({
        fill: "yellow",
        shape: "ring",
        text: this.port_name + " connecting",
      });
      config.color = config.color.slice(1);
      msg.config = config;
      // var command = "grove-rgb-led-strip.py 12 30 1 ff3300";
      var command = "grove-rgb-led-strip.py " + config.port + " " + config.count + " " + config.pattern + " " + config.color;
      msg.payload = command; 
      node.send(msg);
      exec(
        "sudo python -u " +
          path.join(
            __dirname,
            command
          ),
        (err, stdout, stderr) => {
          if (err) {
            console.log(err);
          }
          // console.log(stdout);
          // msg.payload = JSON.parse(stdout);
          msg.payload = stdout;
          this.status({
            fill: "green",
            shape: "dot",
            text: this.port_name + " connected",
          });
          node.send(msg);
        }
      );
    });
  }
  RED.nodes.registerType("grove-rgb-led-strip", GroveRgbLedStrip);
};
