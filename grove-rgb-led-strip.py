#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
# Port to support Grove - WS2813 RGB LED Strip Waterproff - XXX LED/m
#
# Grove Base Hat for the Raspberry Pi, used to connect grove sensors.
# Copyright (C) 2018  Seeed Technology Co.,Ltd.
#
# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
'''
This is the code for
    - `Grove - WS2813 RGB LED Strip Waterproof - 30 LED/m -1m   <https://www.seeedstudio.com/Grove-WS2813-RGB-LED-Strip-Waterproof-30-LED-m-1m-p-3124.html>`_
    - `Grove - WS2813 RGB LED Strip Waterproof - 60 LED/m - 1m  <https://www.seeedstudio.com/Grove-WS2813-RGB-LED-Strip-Waterproof-60-LED-m-1m-p-3126.html>`_
    - `Grove - WS2813 RGB LED Strip Waterproof - 144 LED/m - 1m <https://www.seeedstudio.com/Grove-WS2813-RGB-LED-Strip-Waterproof-144-LED-m-1m-p-3127.html>`_

Examples:

    .. code-block:: python

        import time
        from rpi_ws281x import Color
        from grove.grove_ws2813_rgb_led_strip import GroveWS2813RgbStrip

        # connect to pin 12(slot PWM)
        PIN   = 12
        # For Grove - WS2813 RGB LED Strip Waterproof - 30 LED/m
        # there is 30 RGB LEDs.
        COUNT = 30
        strip = GroveWS2813RgbStrip(PIN, COUNT)

        # Define functions which animate LEDs in various ways.
        def colorWipe(strip, color, wait_ms=50):
            """Wipe color across display a pixel at a time."""
            for i in range(strip.numPixels()):
                strip.setPixelColor(i, color)
                strip.show()
                time.sleep(wait_ms/1000.0)

        print ('Color wipe animations.')
        colorWipe(strip, Color(255, 0, 0))  # Red wipe
        colorWipe(strip, Color(0, 255, 0))  # Blue wipe
        colorWipe(strip, Color(0, 0, 255))  # Green wipe
'''

__all__ = ['GroveWS2813RgbStrip', 'PixelStrip', 'Color']

import time
from rpi_ws281x import PixelStrip, Color

# LED strip configuration
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

class GroveWS2813RgbStrip(PixelStrip):
    '''
    Wrapper Class for Grove - WS2813 RGB LED Strip Waterproof - XXX LED/m

    Args:
        pin(int)  : 12, 18 for RPi
        count(int): strip LEDs count
        brightness(int): optional, set to 0 for darkest and 255 for brightest, default 255
    '''
    def __init__(self, pin, count, brightness = None):
        ws2812_pins = { 12:0, 13:1, 18:0, 19:1}
        if not pin in ws2812_pins.keys():
            print("OneLedTypedWs2812: pin {} could not used with WS2812".format(pin))
            return
        channel = ws2812_pins.get(pin)

        if brightness is None:
            brightness = LED_BRIGHTNESS

        # Create PixelStrip object with appropriate configuration.
        super(GroveWS2813RgbStrip, self).__init__(
            count,
            pin,
            LED_FREQ_HZ,
            LED_DMA,
            LED_INVERT,
            brightness,
            channel
        )

        # Intialize the library (must be called once before other functions).
        self.begin()


# Define functions which animate LEDs in various ways.
def colorWipe(arguments):
    """Wipe color across display a pixel at a time."""
    
    for i in range(arguments[0].numPixels()):
        arguments[0].setPixelColor(i, arguments[1])
        arguments[0].show()
        time.sleep(arguments[2]/1000.0)

def theaterChase(arguments):
    """Movie theater light style chaser animation."""
    for j in range(arguments[3]):
        for q in range(3):
            for i in range(0, arguments[0].numPixels(), 3):
                arguments[0].setPixelColor(i+q, arguments[1])
            arguments[0].show()
            time.sleep(arguments[2]/1000.0)
            for i in range(0, arguments[0].numPixels(), 3):
                arguments[0].setPixelColor(i+q, 0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(arguments):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*arguments[2]):
        for i in range(arguments[0].numPixels()):
            arguments[0].setPixelColor(i, wheel((i+j) & 255))
        arguments[0].show()
        time.sleep(arguments[1]/1000.0)

def rainbowCycle(arguments):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*arguments[2]):
        for i in range(arguments[0].numPixels()):
            arguments[0].setPixelColor(i, wheel((int(i * 256 / arguments[0].numPixels()) + j) & 255))
        arguments[0].show()
        time.sleep(arguments[1]/1000.0)

def theaterChaseRainbow(arguments):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, arguments[0].numPixels(), 3):
                arguments[0].setPixelColor(i+q, wheel((i+j) % 255))
            arguments[0].show()
            time.sleep(arguments[1]/1000.0)
            for i in range(0, arguments[0].numPixels(), 3):
                arguments[0].setPixelColor(i+q, 0)

def off(arguments):
    for i in range(strip.numPixels()):
        arguments[0].setPixelColor(i, Color(0,0,0))
        arguments[0].show()
        time.sleep(0.01)


def main():
    from grove import helper
    from grove.helper import helper
    helper.root_check()

    from grove.helper import SlotHelper
    sh = SlotHelper(SlotHelper.PWM)
    pin = sh.argv2pin(" [led-count] [pattern] [color]")

    import sys

    count = 30
    if len(sys.argv) >= 3:
        count = int(sys.argv[2])

    color = Color(255, 255, 255)
    if len(sys.argv) >= 5:
        hexcolor = sys.argv[4].lstrip('#')
        #print ('Hex Color', hexcolor)
        rgbcolor = tuple(int(hexcolor[i:i+2], 16) for i in (0, 2, 4))
        #print ('RGB Color', rgbcolor)
        color = Color(*rgbcolor)

    wait = 50
    if len(sys.argv) >= 6:
        wait = int(sys.argv[5])

    iterations = 50
    if len(sys.argv) >= 7:
        iterations = int(sys.argv[6])

    strip = GroveWS2813RgbStrip(pin, count)

    patterns = {
        0: {
            "name": off,
            "arguments": (strip,)
        },
        1: {
            "name": colorWipe,
            "arguments": (strip, color, wait),
            "defaults": {
                "color": Color(255, 255, 255),
                "wait": 50
            }
        },
        2: {
            "name": theaterChase,
            "arguments": (strip, color, wait, iterations),
            "defaults": {
                "color": Color(255, 255, 255),
                "wait": 50,
                "interations": 10
            }
        },
        3: {
            "name": rainbow,
            "arguments": (strip, wait, iterations),
            "defaults": {
                "wait": 20,
                "interations": 1
            }
        },
        4: {
            "name": rainbowCycle,
            "arguments": (strip, wait, iterations),
            "defaults": {
                "wait": 20,
                "interations": 5
            }
        },
        5: {
            "name": theaterChaseRainbow,
            "arguments": (strip, wait),
            "defaults": {
                "wait": 20
            }
        }
    }

    if len(sys.argv) >= 4:
        pattern = patterns.get(int(sys.argv[3]), patterns[0])['name']
        arguments = patterns.get(int(sys.argv[3]), patterns[0])['arguments']
    else:
        pattern = patterns[0]['name']
        arguments = patterns[0]['arguments']


    pattern(arguments)
    #colorWipe(strip, Color(255, 0, 0))

    # print ('Color wipe animations.')
    # colorWipe(strip, Color(255, 0, 0))  # Red wipe
    # colorWipe(strip, Color(0, 255, 0))  # Blue wipe
    # colorWipe(strip, Color(0, 0, 255))  # Green wipe
    # print ('Theater chase animations.')
    # theaterChase(strip, Color(127, 127, 127))  # White theater chase
    # theaterChase(strip, Color(127,   0,   0))  # Red theater chase
    # theaterChase(strip, Color(  0,   0, 127))  # Blue theater chase
    # print ('Rainbow animations.')
    # rainbow(strip)
    # rainbowCycle(strip)
    # theaterChaseRainbow(strip)

if __name__ == '__main__':
    main()
