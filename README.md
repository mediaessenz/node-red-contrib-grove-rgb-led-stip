This is a Node-RED node to control a Grove RGB LED strip (https://www.seeedstudio.com/Grove-WS2813-RGB-LED-Strip-Waterproof-30-LED-m-1m.html) connected to the Grove Base Hat for Raspberry Pi (https://wiki.seeedstudio.com/Grove_Base_Hat_for_Raspberry_Pi/).

To get it working you have to install it in Node-RED at least ;-)

After this realize the new node "grove-rgb-led-strip" in the group "grove".
Drop it on your flow, configure it's settings and add a trigger to its input.
Enjoy the lightshow. 

Party on!

The included javascript handles the communication between Node-RED and the python script and hands over the msg.payload and node settings.

January 2021
